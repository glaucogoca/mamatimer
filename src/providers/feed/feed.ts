import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Storage } from '@ionic/storage';
import { Feed } from '../../domain/feed';


@Injectable()
export class FeedProvider {

  public timeNow: Date = new Date();
  public stopped: boolean = true;
  public startDate: Date;
  public seconds: number = 0;
  public minutes: number = 0;
  public hours: number = 0;
  public feed: Feed;

  constructor(public http: HttpClient,
    public storage: Storage,
    private datePipe: DatePipe) {
  }


  save(feed: Feed) {
    let key = this.datePipe.transform(new Date(), 'yyyyMMdd');
    console.log('key to save = ' + key);
    return this.getAllFeeds(key).then(result => {
      if (result) {
        result.push(feed);
        return this.storage.set(key, result);
      } else {
        return this.storage.set(key, [feed]);
      }
    });
  }

  remove(feed: Feed) {
   return new Promise((resolve, reject) => {
      let key = this.datePipe.transform(feed.begin, 'yyyyMMdd');
      this.getAllFeeds(key).then(result => {
        if (result) {
          console.log('trying to remove');
          let newArray = [];
          for (var _i = 0; _i < result.length; _i++) {
            let item: Feed = result[_i];
            if (item.begin.getTime() != feed.begin.getTime()) {
              console.log('not removed ' + item);
              newArray.push(item);
            }
          }
          this.storage.set(key, newArray);
          resolve(newArray);
          console.log("done!");
        }
      })
    });
  }


  getAllFeeds(key) {
    return this.storage.get(key);
  }

  add() {
    this.seconds += 1;
    if (this.seconds === 60) {
      this.minutes += 1
      this.seconds = 0;
    }

    if (this.minutes === 60) {
      this.hours += 1;
      this.minutes = 0;
    }
  }

  public getValue(num) {
    let result = "" + num;
    if (num < 10) {
      result = "0" + num;
    }
    return result;
  }

  public createFeed() {
    this.feed = new Feed();
    this.feed.begin = this.startDate;
    this.feed.hours = this.hours;
    this.feed.minutes = this.minutes;
    this.feed.seconds = this.seconds;
    this.feed.end = new Date();
    this.save(this.feed);
  }

  public resetTimer() {
    this.hours = 0;
    this.minutes = 0;
    this.seconds = 0;
  }


  public updateTimer() {
    this.add();
    if (!this.stopped) {
      setTimeout(() => {
        this.updateTimer();
      }, 1000);
    }
  }

  public updateClock() {
    this.timeNow = new Date();
    if (this.stopped) {
      setTimeout(() => {
        this.updateClock();
      }, 10);
    }
  }
  public getFeedTimerStr() {
    return this.getValue(this.feed.hours) + ':' +
      this.getValue(this.feed.minutes) + ':' +
      this.getValue(this.feed.seconds);
  }

  public getTimerStr() {
    return this.getValue(this.hours) + ':' +
      this.getValue(this.minutes) + ':' +
      this.getValue(this.seconds);
  }


}
