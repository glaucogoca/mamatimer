
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Config } from '../../domain/config';

const STORAGE_KEY = 'config';

@Injectable()
export class ConfigProvider {



  constructor(public http: HttpClient,
              private storage: Storage) {

      this.getConfig().then( result => {
        if(!result){
         this.createNewConfig();
        }
      });
  }

  public createNewConfig(){
    let config  = new Config();
    config.alarm = true;
    config.alarmPeriod = 2;
    config.vibrate = true;
    this.save(config);
    return config;
  }

  public getConfig(){
    return this.storage.get(STORAGE_KEY);
  }

  public save(config:Config){
    this.storage.set(STORAGE_KEY, config);
  }
}
