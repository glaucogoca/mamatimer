export class Config {
  vibrate: boolean;
  alarm: boolean;
  alarmPeriod: number;
}
