import { ChartsModule } from 'ng2-charts/ng2-charts';
import { NgModule } from '@angular/core';



@NgModule({
  declarations: [
  ],
  imports: [
    ChartsModule
  ],
})
export class StatsPageModule {}
