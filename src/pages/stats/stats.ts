import { Feed } from './../../domain/feed';
import { DatePipe } from '@angular/common';
import { FeedProvider } from './../../providers/feed/feed';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';




@IonicPage()
@Component({
  selector: 'page-stats',
  templateUrl: 'stats.html',
})
export class StatsPage {

  public lineChartData: Array<any>; /*= [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Breastfeeding' },
  ];*/
  public lineChartLabels: Array<any>;// = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartOptions: any = {
    responsive: true
  };
  public lineChartColors: Array<any> = [
    { // grey
      backgroundColor: 'rgba(253, 140, 233,0.2)',
      borderColor: '#fd8ce9',
      pointBackgroundColor: '#fd8ce9',
      pointBorderColor: '#fd8ce9',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend: boolean = true;
  public lineChartType: string = 'line';
  showGraph = false;
  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  dateGraph;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private feedProvider: FeedProvider,
    private datePipe: DatePipe) {
      let myDate = new Date();
      myDate.setHours(0, -myDate.getTimezoneOffset(), 0, 0);
      console.log(myDate);
      this.dateGraph = myDate.toISOString();
      this.findAllByDay();
  }



  findAllByDay(){
    console.log("findAllByDay");
    let myDate = new Date(this.dateGraph);
    let mls = myDate.getTime() + 86400000;
    let key = this.datePipe.transform(new Date(mls), 'yyyyMMdd');
    console.log(key);
    this.feedProvider.getAllFeeds(key).then(result  => {
      console.log("Result");
      if(result){
        console.log("Result = true");
        let newLabels = [];
        let newValues = [];
        for(var _i=0;_i< result.length;_i++){
          let item: Feed = result[_i];
          console.log(item);
          let hour = this.datePipe.transform(item.begin,'HH');
          newLabels.push(hour);

          let value = item.minutes + (60 * item.hours);
          if(item.seconds > 0){
            value += item.seconds / 60;
          }

          newValues.push(value);
        }
        console.log(newLabels);
        console.log(newValues);
        this.lineChartData = [
          { data: newValues, label: 'Breastfeeding' },
        ];
        this.lineChartLabels = newLabels;
        this.showGraph = true;
      }else{
        this.showGraph = false;
        console.log("showGraph = false");
      }
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StatsPage');
  }
  chooseDate(){
    console.log('date changed');
    this.findAllByDay();
  }

}
