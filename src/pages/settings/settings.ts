import { Config } from './../../domain/config';
import { ConfigProvider } from './../../providers/config/config';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { WheelSelector } from '@ionic-native/wheel-selector';

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  config: Config = new Config();


  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private configProvider: ConfigProvider,
    private selector: WheelSelector) {
    this.configProvider.getConfig().then(result => {
      if (result) {
        this.config = result;
      } else {
        this.config = this.configProvider.createNewConfig();
      }
    }
    );

  }


  simpleExample() {

    let jsonData = {
      numbers: [
        { description: "1" },
        { description: "2" },
        { description: "3" }
      ],
    };

    this.selector.show({
      title: "Select alarm delay",
      items: [
        jsonData.numbers
      ],
    }).then(result => {
        this.config.alarmPeriod = result[0].index + 1;
        this.saveConfig();
      },
      err => console.log('Error occurred while getting result: ', err)
    );
  }

  saveConfig() {
    this.configProvider.save(this.config);
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }

}
