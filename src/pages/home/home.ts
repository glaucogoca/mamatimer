import { Config } from './../../domain/config';
import { ConfigProvider } from './../../providers/config/config';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Feed } from '../../domain/feed';
import { FeedProvider } from '../../providers/feed/feed';
import { BackgroundMode } from '@ionic-native/background-mode';




import { AdMobFree, AdMobFreeBannerConfig, AdMobFreeInterstitialConfig } from '@ionic-native/admob-free';
import { LocalNotifications, ELocalNotificationTriggerUnit } from '@ionic-native/local-notifications';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {


  timeNow : Date =    new Date();
  state = "backimageStopped";


  stopped: boolean = true;
  startDate : Date;
  seconds : number = 0;
  minutes : number = 0;
  hours : number = 0;
  feed : Feed;

  constructor(public navCtrl: NavController,
              public admob: AdMobFree,
              public feedProvider: FeedProvider,
              public backGroundMode : BackgroundMode,
              public localNotifications : LocalNotifications,
              private configProvider: ConfigProvider) {
    this.feedProvider.updateClock();
    this.backGroundMode.setDefaults({
      title : "Mama Timer",
      text: "Breastfeeding "+this.feedProvider.getTimerStr()
    });


    if(this.feedProvider.stopped === true){
      this.state = "backimageStopped";
    } else {
      this.state = "backimageRunning";
    }

    this.showBanner();

  }
/*
  resetTimer(){
    this.hours = 0;
    this.minutes = 0;
    this.seconds = 0;
  }


  add(){
    this.seconds += 1;
    if(this.seconds === 60){
      this.minutes += 1
      this.seconds = 0;
    }

    if(this.minutes === 60){
      this.hours += 1;
      this.minutes = 0;
    }
  }

  getValue(num){
    let result = "" + num;
    if(num < 10){
      result = "0"+num;
    }
    return result;
  }*/


  public start() {
    this.backGroundMode.enable();
    this.feedProvider.stopped = false;
    this.feedProvider.startDate = new Date();
    this.feedProvider.updateTimer();

    this.state = "backimageRunning";

    /*this.stopped = false;
    this.startDate = new Date();
    this.updateTimer(); */
  }

/*  createFeed(){
    this.feed = new Feed();
    this.feed.begin = this.startDate;
    this.feed.hours = this.hours;
    this.feed.minutes = this.minutes;
    this.feed.seconds = this.seconds;
    this.feed.end = new Date();
    this.feedProvider.save(this.feed);
  } */

  public stop() {
    this.backGroundMode.disable();

    this.state = "backimageStopped";

    this.feedProvider.stopped = true;
    this.feedProvider.createFeed();
    this.feedProvider.resetTimer();
    this.feedProvider.updateClock();

    //this.stopped = true;
    //this.feedProvider.createFeed();
    //this.resetTimer();
    //this.updateClock();


    this.configProvider.getConfig().then(config =>{
      this.localNotifications.clearAll();
      console.log(config);
      if(config){
        if(config.alarm){
          console.log("Set alarm");
          this.scheduleAlarm(config);
        }else{
          console.log("Alarm not set");
        }
      } else {
        let config = this.configProvider.createNewConfig();
        this.scheduleAlarm(config);
      }
    });
  }

  scheduleAlarm(config:Config){
    this.localNotifications.schedule({
      id: 1,
      title: 'MamaTimer',
      text: 'Don\'t forget to nurse your baby! ;)',
      led: 'FF0000',
      sound: null,
      vibrate: config.vibrate,
      icon: 'res://icon',
      trigger : { in: config.alarmPeriod, unit: ELocalNotificationTriggerUnit.HOUR }
    });
    // this.localNotifications.schedule({
    //   id: 2,
    //   title: 'MamaTimer',
    //   text:  'It\'s time to nurse your baby!',
    //   sound: 'file://sound.mp3',
    //   led: 'FF0000',
    //   vibrate: true,
    //   icon: 'res://icon',
    //   data: { secret: 'test' }
    // });

  }

  /*updateTimer() {
    this.add();
    if(!this.stopped){
      setTimeout(() => {
        this.updateTimer();
      },1000);
    }
  }

  updateClock(){
    this.timeNow = new Date();
    if(this.stopped) {
      setTimeout(() => {
        this.updateClock();
      },10);
    }
  }*/



  showBanner() {

    let bannerConfig: AdMobFreeBannerConfig = {
        isTesting: true, // Remove in production
        autoShow: true,
        id: 'ca-app-pub-9271576809981176/6119759793'
    };

    this.admob.banner.config(bannerConfig);

    this.admob.banner.prepare().then(() => {
        // success
    }).catch(e => console.log(e));

  }

  launchInterstitial() {

    let interstitialConfig: AdMobFreeInterstitialConfig = {
        isTesting: true, // Remove in production
        autoShow: true,
        id: 'ca-app-pub-9271576809981176/6119759793'
    };

    this.admob.interstitial.config(interstitialConfig);

    this.admob.interstitial.prepare().then(() => {
        // success
    });

}
}
