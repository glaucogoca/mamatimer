import { DatePipe } from '@angular/common';
import { Component } from '@angular/core';
import { Feed } from '../../domain/feed';
import { FeedProvider } from '../../providers/feed/feed';
import { NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';



@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {


  items: Array<Feed>;
  dateGraph;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public feedProvider: FeedProvider,
              private datePipe: DatePipe,
              private alertCtrl: AlertController) {

    let myDate = new Date();
    myDate.setHours(0, -myDate.getTimezoneOffset(), 0, 0);
    console.log(myDate);
    this.dateGraph = myDate.toISOString();
    console.log(this.dateGraph);
    this.findAllByDay();
  }

  chooseDate(){
    console.log('date changed');
    this.findAllByDay();
  }

  findAllByDay(){
    console.log(this.dateGraph);
    let myDate = new Date(this.dateGraph);
    let mls = myDate.getTime() + 86400000;
    let key = this.datePipe.transform(new Date(mls), 'yyyyMMdd');
    console.log(key);
    this.feedProvider.getAllFeeds(key).then(result => {
      this.items = result;
      console.log(this.items);
    });
  }

  removeItem(item:Feed){
      this.feedProvider.remove(item).then(() => {
        console.log("It works");
        this.chooseDate();
      });
  }

  showAlert(item:Feed){
    let alert = this.alertCtrl.create({
      title: 'Remove',
      message: 'Do you want to remove this record?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {

              this.removeItem(item);

          }
        }
      ]
    });

    alert.present();

  }

  getValue(num){
    let result = "" + num;
    if(num < 10){
      result = "0"+num;
    }
    return result;
  }
}
